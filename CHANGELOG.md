# Change Log
All notable changes to this project will be documented in this file.
 
## 2024-02-20
 
### Added
- [4f6079f9325f72a74f8105c53518f92653dd8feb](https://gitlab.com/AlexandreMartel/warty-dst-character-mod/-/commit/4f6079f9325f72a74f8105c53518f92653dd8feb) - Added a working version of Warty with all the assets needed for the character.
 
## 2024-02-28
  
### Added
- [b9116e7000c2f12d35f7e7c290332eba9697b2da](https://gitlab.com/AlexandreMartel/warty-dst-character-mod/-/commit/b9116e7000c2f12d35f7e7c290332eba9697b2da) - Added a 
frog house recipe for the character Warty.
  
- [f2e37c860acbadf6281b0fc0f54d6ae8f4a82abb](https://gitlab.com/AlexandreMartel/warty-dst-character-mod/-/commit/f2e37c860acbadf6281b0fc0f54d6ae8f4a82abb) - Added documentation based on the Lua style Guide. 

More info : https://github.com/luarocks/lua-style-guide 

### Changed
Changed some character assets.

### Fixed
Fixed some animations problems. 

## 2024-04-20
 
### Added
- [1781aa22922e4f735872de61e3e7dc7631d3427f](https://gitlab.com/AlexandreMartel/warty-dst-character-mod/-/commit/1781aa22922e4f735872de61e3e7dc7631d3427f) - Added a new perishable weapon for the character Warty.
   
### Changed
- Changed the delete.bat script to include the new assets.
- Changed the warty.lua prefab to add the new weapon recipe.
- Changed the modmain.lua file to include all the assets and code for the new weapon recipe.

## 2024-04-30
  
### Added
- [3b01d703dbf2d19d00704bb5ae36cc4cfbd77df1](https://gitlab.com/AlexandreMartel/warty-dst-character-mod/-/commit/3b01d703dbf2d19d00704bb5ae36cc4cfbd77df1) - Added unit tests using luanit
