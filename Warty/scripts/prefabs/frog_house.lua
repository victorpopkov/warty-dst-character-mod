-- Load required modules
require "prefabutil"
require "recipes"
require "modutil"

-- Define assets used by the prefab
local assets =
{
    Asset("ANIM", "anim/frog_house.zip"),
    Asset("IMAGE", "images/inventory_images/frog_house.tex"),
    Asset("ATLAS", "images/inventory_images/frog_house.xml"),
}

-- Function to handle the "hammered" event
local function onhammered(inst, worker)
    if inst:HasTag("fire") and inst.components.burnable then
        inst.components.burnable:Extinguish()
    end
    SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:Remove()
end

-- Function to handle the "built" event
local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", true)
    inst.SoundEmitter:PlaySound("common/frog_sounds/toad_house")
end

-- Main function to create the prefab entity
local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 0.66)

    inst:AddTag("structure")

    -- Add minimap icon
    local minimap = inst.entity:AddMiniMapEntity()
    minimap:SetIcon("frog_house.tex")

    -- Set animation properties
    inst.AnimState:SetBank("frog_house")
    inst.AnimState:SetBuild("frog_house")
    inst.AnimState:PlayAnimation("idle")

    -- Return if not in master simulation
    if not TheWorld.ismastersim then
        return inst
    end

    -- Define description for inspection
    STRINGS.CHARACTERS.GENERIC.DESCRIBE.FROG_HOUSE = "It's a house for frogs."

    -- Add loot dropper and inspectable components
    inst:AddComponent("lootdropper")
    inst:AddComponent("inspectable")

    -- Add workable component to handle onhammered event
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.onhammered)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(onhammered)

    return inst
end

-- Return prefab entity and placer
return Prefab("common/frog_house", fn, assets),
MakePlacer("common/frog_house_placer", "frog_house", "frog_house", "idle")
