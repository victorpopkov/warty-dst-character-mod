-- Load the module to create player characters
local MakePlayerCharacter = require "prefabs/player_common"

-- Define assets used by the character
local assets = {
    Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
    Asset("SCRIPT", "scripts/prefabs/skilltree_warty.lua")
}

-- Define the character's stats
TUNING.WARTY_HEALTH = 150
TUNING.WARTY_HUNGER = 150
TUNING.WARTY_SANITY = 200

-- Define the character's starting inventory
TUNING.GAMEMODE_STARTING_ITEMS.DEFAULT.WARTY = {
    "flint",
    "flint",
    "twigs",
    "twigs",
}

-- Create a flattened list of starting inventory prefabs
local start_inv = {}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
    start_inv[string.lower(k)] = v.WARTY
end
local prefabs = FlattenTree(start_inv, true)

-- Function to be called when the character becomes human
local function onbecamehuman(inst)
    -- Set speed when not a ghost (optional)
    inst.components.locomotor:SetExternalSpeedMultiplier(inst, "warty_speed_mod", 1)
end

-- Function to be called when the character becomes a ghost
local function onbecameghost(inst)
    -- Remove speed modifier when becoming a ghost
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "warty_speed_mod")
end

-- Function to be called when loading or spawning the character
local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end

-- Function to initialize for both server and client
local common_postinit = function(inst)
    -- Set minimap icon
    inst.MiniMapEntity:SetIcon("warty.tex")
end

-- Function to initialize for the server only
local master_postinit = function(inst)
    -- Set starting inventory
    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

    -- Choose which sounds this character will play
    inst.soundsname = "willow"

    -- Set stats
    inst.components.health:SetMaxHealth(TUNING.WARTY_HEALTH)
    inst.components.hunger:SetMax(TUNING.WARTY_HUNGER)
    inst.components.sanity:SetMax(TUNING.WARTY_SANITY)

    -- Set damage multiplier (optional)
    inst.components.combat.damagemultiplier = 1

    -- Set hunger rate (optional)
    inst.components.hunger.hungerrate = 1 * TUNING.WILSON_HUNGER_RATE

    -- Call onload function when loading or spawning
    inst.OnLoad = onload
    inst.OnNewSpawn = onload

    -- Register frog house recipe
    inst.components.builder:AddRecipe("frog_house")
    inst.components.builder:AddRecipe("leap_frogger")
end

-- Return the player character prefab with initialization functions
return MakePlayerCharacter("warty", prefabs, assets, common_postinit, master_postinit, prefabs)
