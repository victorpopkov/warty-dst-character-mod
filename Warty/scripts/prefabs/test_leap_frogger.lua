﻿luaunit = require('luaunit')

-- Mock Don't Starve Together specific functions
function Asset() end
function CreateEntity() return {} end
function OnLoad() end
function UpdateDamage() end

function Prefab()
    return {
        OnLoad = OnLoad,
        UpdateDamage = UpdateDamage,
    }
end

-- Mock TUNING variable
TUNING = {
    HAMBAT_DAMAGE = 10,
    HAMBAT_MIN_DAMAGE_MODIFIER = 0.5,
    PERISH_MED = 10,
    LEAP_FROGGER_DAMAGE = 10,
}

-- Mock leap_frogger module
leap_frogger = require('leap_frogger')

TestLeapFrogger = {} -- The test class

function TestLeapFrogger:setUp()
    self.leap_frogger = leap_frogger
end

function TestLeapFrogger:testUpdateDamage()
    local inst = {}
    inst.components = {}
    inst.components.perishable = { GetPercent = function() return 0.5 end }
    inst.components.weapon = { SetDamage = function(self, dmg) self.damage = dmg end, damage = 5 }

    self.leap_frogger.UpdateDamage(inst)
    luaunit.assertEquals(inst.components.weapon.damage, TUNING.HAMBAT_DAMAGE * 0.5)
end

function TestLeapFrogger:testOnLoad()
    local inst = {}
    inst.components = {}
    inst.components.perishable = { GetPercent = function() return 0.5 end }
    inst.components.weapon = { SetDamage = function(self, dmg) self.damage = dmg end, damage = 5 }

    self.leap_frogger.OnLoad(inst)
    luaunit.assertEquals(inst.components.weapon.damage, TUNING.HAMBAT_DAMAGE * 0.5)
end


os.exit(luaunit.LuaUnit.run())
